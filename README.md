stm8flash.AppImage
==================

This project uses buildstream and bst-tools to package the stm8flash project into an AppImage.

It requires a master version of buildstream to build the AppImage.

For x86-64:
```
bst -o arch x86-64 --max-jobs=$(nproc) build stm8flash-appimage.bst
```

For aarch64:
```
bst -o arch aarch64 --max-jobs=$(nproc) build stm8flash-appimage.bst
```
